# my classes
from parameters import *
from player import Player
from monster import Monster
from tools import Tool, Effect


class Game:
    def __init__(self, level, inventory, weapon_name):
        self.monsters, self.tools, self.items, self.monsters_bullets = [], [], [], []
        self.map = [self.monsters, self.tools, self.items, self.monsters_bullets]
        self.player = Player(self.map, inventory, weapon_name)

        # self.tools.append(Tool("mana", 100, 100))
        # self.monsters.append(Monster("shooter", 0, 0, self.player, self.map, 100))

        self.wave = 1
        self.monsters_spawn = 0

        self.next_wave = True
        self.loose = False

        self.level = level

    def spawn_monster(self):
        if pxl.frame_count % 240 == 0:
            if self.next_wave:
                self.next_wave = False
            if self.monsters_spawn < self.wave * 2 + 1:
                for k in range(self.wave // 2 + 1):
                    # choose a random place on the screen sides
                    rnd = randint(0, 1)
                    x, y = [randint(-PYXEL_SIZE, SCREEN_SIZE[0]), choice([-PYXEL_SIZE, SCREEN_SIZE[0]])][rnd], \
                        [choice([-PYXEL_SIZE, SCREEN_SIZE[1]]), randint(0, SCREEN_SIZE[1])][rnd]

                    name = MONSTERS[0]
                    for i in range(1, len(MONSTERS)):
                        if randint(1, i * 4) == 1:
                            name = MONSTERS[i]
                            break

                    self.monsters.append(Monster(name, x, y, self.player, self.map, self.wave))
                    self.monsters_spawn += 1

            elif self.monsters == []:
                self.next_wave = True
                self.wave += 1
                self.monsters_spawn = 0

    def update(self):
        if not self.loose:
            # PLAYER
            self.player.update()

            # player dies
            if self.player.life <= 0:
                self.loose = True

            # ITEMS
            for item in self.items:
                item.update()

                if item.life <= 0:
                    if item.name == "teleport":
                        self.player.number_teleport -= 1
                    elif item.name == "beacon":
                        item.beacon()
                    self.items.remove(item)

            # MONSTER
            # spawn a new monster
            self.spawn_monster()

            # monster's bullets
            for bullet in self.monsters_bullets:
                bullet.move()

                # out of the screen
                if not (0 < bullet.x < SCREEN_SIZE[0] and 0 < bullet.y < SCREEN_SIZE[1]):
                    self.monsters_bullets.remove(bullet)

                # damage an entity
                elif bullet in self.monsters_bullets:
                    for entity in [self.player] + self.items:
                        if collision([entity], [bullet.x, bullet.y, bullet.width, bullet.height]):
                            entity.life -= bullet.damage
                            for b_effect in bullet.effects:
                                for effect in entity.effects:
                                    if effect.name == b_effect[0]:
                                        effect.time = 0
                                        effect.update()
                                        break
                                entity.effects.append(Effect(b_effect[0], entity, b_effect[1]))
                            self.monsters_bullets.remove(bullet)
                            break

            for monster in self.monsters:
                monster.update()

                # kill monster
                if monster.life <= 0:
                    if randint(1, 50) == 1:
                        self.tools.append(Tool("heart", monster.x, monster.y))
                    elif randint(1, 60) == 1:
                        self.tools.append(Tool("mana", monster.x, monster.y))
                    elif randint(1, 300) == 1:
                        self.tools.append(Tool("nuke", monster.x, monster.y))

                    self.level[1] += monster.xp
                    self.monsters.remove(monster)
        else:
            if pxl.btn(pxl.KEY_SPACE):
                self.monsters, self.tools, self.items, self.monsters_bullets = [], [], [], []
                self.map = [self.monsters, self.tools, self.items, self.monsters_bullets]
                self.player = Player(self.map, self.player.inventory.contain, self.player.weapon.name)

                self.wave = 1
                self.monsters_spawn = 0

                self.loose = False

    def draw(self):
        if not self.loose:
            # background
            pxl.bltm(0, 0, 0, 0, 0, SCREEN_SIZE[0], SCREEN_SIZE[1])

            # map
            for tab in self.map:
                for entity in tab:
                    entity.display()

            for monster in self.monsters:
                monster.display()

            # PLAYER DISPLAY (with all his components)
            self.player.display()

            # display wave
            txt = f"{self.wave} wave"
            if self.next_wave:
                pxl.rect(SCREEN_SIZE[0] - len(txt) * 4 - 4, 2, len(txt) * 4 + 3, 10, 10)
                pxl.rect(SCREEN_SIZE[0] - len(txt) * 4 - 3, 3, len(txt) * 4 + 1, 8, 9)
            pxl.text(SCREEN_SIZE[0] - len(txt) * 4 - 2, 4, txt, 7)

            # display the level
            txt = f"Level {self.level[0]}"
            pxl.text(2, SCREEN_SIZE[1] - 6, txt, 7)
            # display the xp bar
            pxl.rect(4 + len(txt) * 4, SCREEN_SIZE[1] - 6, SCREEN_SIZE[0] - 6 - len(txt) * 4, 5, 1)
            pxl.rect(5 + len(txt) * 4, SCREEN_SIZE[1] - 5, int((SCREEN_SIZE[0] - 8 - len(txt) * 4) * (self.level[1] /
                                                                    (150 * self.level[0]))), 3, 11)
        else:
            txt = "press -SPACE- to retry"
            pxl.text(SCREEN_SIZE[0] // 2 - len(txt) * 2, SCREEN_SIZE[1] // 2 - 2, txt, 7)
