# my classes
from parameters import *
from tools import Item, Effect, Weapon


class Inventory:
    def __init__(self, elements):
        self.size = len(elements)

        self.contain = elements

        self.y = SCREEN_SIZE[1] - PYXEL_SIZE - 9

        # put the right coordonates to display in the inventory
        for i in range(len(self.contain)):
            if self.contain[i] is not None:
                self.contain[i].x, self.contain[i].y = 3 + i * (3 + PYXEL_SIZE), self.y

    def use_item(self):
        keys = [KEYS[f"item {k}"] for k in range(1, 11)]
        for i in range(self.size):
            if pxl.btnp(pxl.__getattribute__(keys[i])):
                return self.contain[i]

    def add_item(self, item):
        if None in self.contain:
            for x in range(self.size-1):
                if self.contain[x] is None:
                    item.x, item.y = 3 + x * PYXEL_SIZE + x * 3, self.y
                    self.contain[x] = item
                    return True
        return False

    def display(self):
        # inventory background
        for x in range(self.size):
            pxl.rect(2 + x * PYXEL_SIZE + x * 3, self.y - 1, PYXEL_SIZE + 2, PYXEL_SIZE + 2, 9)
            pxl.rect(3 + x * PYXEL_SIZE + x * 3, self.y, PYXEL_SIZE, PYXEL_SIZE, 4)

        # my items
        for x in range(self.size):
            if self.contain[x] is not None:
                self.contain[x].display(True)


class Player:
    def __init__(self, map, inventory, weapon_name):
        self.name = "player"

        # position & size
        self.x, self.y = PLAYER_SPAWN
        self.width, self.height = PLAYER_SIZE

        # stats
        self.speed = 3
        self.effects = []
        self.max_life = PLAYER_HEART
        self.life = self.max_life
        self.max_mana = PLAYER_MANA
        self.mana = self.max_mana

        self.number_teleport = 0

        self.inventory = Inventory(inventory)

        # weapon
        self.weapon = Weapon(weapon_name, self, map, self)
        self.attacked = False

        # map
        self.map = map
        self.monsters = map[0]
        self.tools = map[1]
        self.items = map[2]

    def use_item(self):
        item = self.inventory.use_item()
        if item is not None and self.mana >= item.mana_cost:
            if hasattr(item, "effect"):
                name = item.name + "_" + item.effect
            else:
                name = item.name
            new_item = Item(name, pxl.mouse_x - PYXEL_SIZE//2, pxl.mouse_y-PYXEL_SIZE // 2, self, self.map)
            if not collision([self] + self.monsters + self.items, [new_item.x, new_item.y, new_item.width,
                                                                   new_item.height]):
                if new_item.name != "teleport" or self.number_teleport < 2:
                    if item.name == "beacon":
                        self.items.insert(0, new_item)
                    else:
                        self.items.append(new_item)
                    self.mana -= item.mana_cost

    def use_tool(self):
        for tool in self.tools:
            if collision([tool], [self.x, self.y, self.width, self.height]):
                if tool.name == "heart":
                    for effect in self.effects:
                        if effect.name == "heal":
                            effect.time = 0
                            effect.update()
                    self.effects.append(Effect("heal", self, HEART_GIVE * 6))
                elif tool.name == "mana":
                    for effect in self.effects:
                        if effect.name == "mana":
                            effect.time = 0
                            effect.update()
                    self.effects.append(Effect("mana", self, MANA_GIVE * 30))
                elif tool.name == "nuke":
                    for monster in self.monsters:
                        monster.life -= tool.damage
                self.tools.remove(tool)

    def move(self):
        # move left & right
        if pxl.btn(pxl.__getattribute__(KEYS["left"])):
            if self.speed <= self.x and not collision(self.monsters + different_name(self.items, ["teleport"]),
                                                      [self.x - self.speed, self.y, self.width, self.height]):
                self.x -= self.speed
            else:
                for k in range(self.speed - 1):
                    if 1 <= self.x and not collision(self.monsters + different_name(self.items, ["teleport"]),
                                                              [self.x - 1, self.y, self.width, self.height]):
                        self.x -= 1
                    else:
                        break
        elif pxl.btn(pxl.__getattribute__(KEYS["right"])):
            if (SCREEN_SIZE[0] >= self.x + self.speed + self.width and not collision(self.monsters +
               different_name(self.items, ["teleport"]), [self.x + self.speed, self.y, self.width, self.height])):
                self.x += self.speed
            else:
                for k in range(self.speed - 1):
                    if SCREEN_SIZE[0] >= self.x + 1 + self.width and not collision(self.monsters + different_name(self.items, ["teleport"]),
                                                              [self.x + 1, self.y, self.width, self.height]):
                        self.x += 1
                    else:
                        break

        # move up & down
        if pxl.btn(pxl.__getattribute__(KEYS["up"])):
            if self.speed <= self.y and not collision(self.monsters + different_name(self.items, ["teleport"]),
                                                      [self.x, self.y - self.speed, self.width, self.height]):
                self.y -= self.speed
            else:
                for k in range(self.speed - 1):
                    if 1 <= self.y and not collision(self.monsters + different_name(self.items, ["teleport"]),
                            [self.x, self.y - 1, self.width, self.height]):
                        self.y -= 1
                    else:
                        break
        elif pxl.btn(pxl.__getattribute__(KEYS["down"])):
            if (SCREEN_SIZE[1] >= self.y + self.speed + self.height and not collision(self.monsters +
               different_name(self.items, ["teleport"]), [self.x, self.y + self.speed, self.width, self.height])):
                self.y += self.speed
            else:
                for k in range(self.speed - 1):
                    if SCREEN_SIZE[1] >= self.y + 1 + self.height and not collision(self.monsters + different_name(self.items, ["teleport"]),
                            [self.x, self.y + 1, self.width, self.height]):
                        self.y += 1
                    else:
                        break

    def update(self):
        # update the effects
        for effect in self.effects:
            effect.update()

        # move
        self.move()

        # shoot
        self.weapon.update()
        # self.shoot()

        # ITEMS
        self.use_item()

        # mana regeneration
        if pxl.frame_count % 30 == 0 and self.mana < self.max_mana:  # +1 by 0.5 sec
            self.mana += 1

        # inventory change the selected item
        self.inventory.use_item()

        # use tool
        self.use_tool()

    def display(self):
        # display the player
        if self.attacked:
            y = PYXEL_SIZE
        else:
            y = 0

        if pxl.mouse_x < self.x - self.width:  # left
            # display the bullets
            self.weapon.display()

            pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 2, y, -self.width, self.height, 3)
        elif pxl.mouse_x > self.x + self.width * 2:  # right
            # display the bullets
            self.weapon.display()

            pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 2, y, self.width, self.height, 3)
        else:  # look vertically
            if pxl.mouse_y < self.y + self.height:  # up
                # display the bullets
                self.weapon.display()

                pxl.blt(self.x, self.y, 0, PYXEL_SIZE, y, self.width, self.height, 3)
            else:  # under
                pxl.blt(self.x, self.y, 0, 0, y, self.width, self.height, 3)

                # display the bullets
                self.weapon.display()

        # display life bar
        pxl.rect(4, 4, 100, 10, 1)
        pxl.rect(5, 5, self.life * 100 // self.max_life - 2, 8, 8)
        # display life I have
        pxl.text(106, 6, str(self.life), 7)

        # display the mana
        pxl.rect(4, 15, 100, 10, 1)
        pxl.rect(5, 16, self.mana * 100 // self.max_mana - 2, 8, 5)
        pxl.text(106, 17, str(self.mana), 7)

        # display the player effect
        for i in range(len(self.effects)):
            self.effects[i].display(4 + i * PYXEL_SIZE + i * 3, 27)

        # display the inventory
        self.inventory.display()
