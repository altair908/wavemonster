# my classes
from parameters import *
from tools import Weapon


class Monster:
    def __init__(self, name, x, y, player, map, wave):
        self.name = name
        self.x, self.y = x, y

        self.width, self.height = PYXEL_SIZE, PYXEL_SIZE

        self.player = player
        self.items = map[2]

        self.wave = wave

        self.effects = []

        self.xp = 5

        # stats
        if self.name == "biscuit":
            self.max_life = 130
            self.speed = 2
        elif self.name == "shooter":
            self.max_life = 80
            self.speed = 1
            self.xp = 10
        elif self.name[:8] == "sorcerer":
            self.effect = self.name[9:]
            if self.effect == "poison":
                self.effect_time = 240  # 8 sec
            else:
                self.effect_time = 120  # 4 sec

            self.name = "sorcerer"

            self.max_life = 60
            self.speed = 1

            self.xp = 15

        self.weapon = Weapon("", self, map, self.player)

        # upgrade the difficulty by the number of wave
        self.max_life += self.wave // 2
        self.weapon.damage += self.weapon.damage // 100 * self.wave

        self.attacked, self.target = 0, (SCREEN_SIZE[0] // 2, SCREEN_SIZE[1] // 2)
        self.life = self.max_life
        self.time = self.weapon.reset_time

    def can_attack(self):
        if self.time == 0:
            self.time = self.weapon.reset_time
            return True
        else:
            self.time -= 1
            return False

    def move(self):
        # the closest target
        dist, move_x, move_y = None, None, None
        for entity in [self.player] + self.items:
            temp_dist = pxl.sqrt((entity.x - self.x) ** 2 + (entity.y - self.y) ** 2)
            if dist is None or temp_dist < dist:
                self.target = (entity.x, entity.y)
                dist = temp_dist
                move_x, move_y = entity.x - self.x, entity.y - self.y

        # monsters try to rush the player
        if dist is not None:
            if (self.name not in ["shooter", "sorcerer"] or dist > self.weapon.range - 30 or not (PYXEL_SIZE < self.x < SCREEN_SIZE[0] -
               PYXEL_SIZE and PYXEL_SIZE < self.y < SCREEN_SIZE[1] - PYXEL_SIZE)):
                while abs(move_x) > self.speed or abs(move_y) > self.speed:
                    m_x, m_y = move_x, move_y
                    move_x //= 2
                    move_y //= 2
                    if (move_x, move_y) == (m_x, m_y):
                        break
                x, y = self.x, self.y
                self.x += move_x
                self.y += move_y
                # collision with player
                if collision([self.player] + self.items, [self.x, self.y, self.width, self.height]):
                    self.x, self.y = x, y

    def update(self):
        if self.attacked > 0:
            self.attacked -= 1

        # update the effects
        for effect in self.effects:
            effect.update()

        self.move()

        self.weapon.update()

    def display(self):
        if self.name == "biscuit":
            if self.attacked != 0:
                y = PYXEL_SIZE * 3
            else:
                y = PYXEL_SIZE * 2

            if self.target[0] < self.x:  # left
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 2, y, -self.width, self.height, 3)
            elif self.target[0] > self.x + self.width - 1:  # right
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 2, y, self.width, self.height, 3)
            else:  # look vertically
                if self.target[1] < self.y + self.height:  # under
                    pxl.blt(self.x, self.y, 0, PYXEL_SIZE, y, self.width, self.height, 3)
                else:  # up
                    pxl.blt(self.x, self.y, 0, 0, y, self.width, self.height, 3)
        elif self.name == "shooter":
            if self.attacked != 0:
                y = PYXEL_SIZE * 5
            else:
                y = PYXEL_SIZE * 4
            if self.target[0] < self.x - self.width:  # left
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 2, y, -self.width, self.height, 3)
            elif self.target[0] > self.x + self.width * 2 - 1:  # right
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 2, y, self.width, self.height, 3)
            else:  # look vertically
                if self.target[1] < self.y + self.height:  # under
                    pxl.blt(self.x, self.y, 0, PYXEL_SIZE, y, self.width, self.height, 3)
                else:  # up
                    pxl.blt(self.x, self.y, 0, 0, y, self.width, self.height, 3)
        elif self.name == "sorcerer":
            if self.attacked != 0:
                y = PYXEL_SIZE * 7
            else:
                y = PYXEL_SIZE * 6
            if self.target[0] < self.x - self.width:  # left
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 2, y, -self.width, self.height, 3)
            elif self.target[0] > self.x + self.width * 2:  # right
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 2, y, self.width, self.height, 3)
            else:  # look vertically
                if self.target[1] < self.y + self.height:  # under
                    pxl.blt(self.x, self.y, 0, PYXEL_SIZE, y, self.width, self.height, 3)
                else:  # up
                    pxl.blt(self.x, self.y, 0, 0, y, self.width, self.height, 3)

        # display his life
        pxl.rect(self.x - 2, self.y - 4, self.width + 5, 3, 1)
        pxl.rect(self.x - 1, self.y - 3, self.life * (self.width + 4) // self.max_life - 1, 1, 8)
