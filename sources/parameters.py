# use in other files
import pyxel as pxl
from random import randint, choice
from copy import deepcopy
# use in this file
import csv

PYXEL_SIZE = 16

# SCREEN
SCREEN_SIZE = (PYXEL_SIZE*32, PYXEL_SIZE*20)

# MONSTERS
MONSTERS = ["biscuit", "shooter", "sorcerer_slow", "sorcerer_poison", "sorcerer_burn"]

# KEYS
KEYS = {}  # can change !!!!!
with open("option.csv", "r+") as file:
    reader = csv.reader(file, delimiter=";")
    for row in reader:
        KEYS[row[0]] = row[1]

# PLAYER SPAWNING
PLAYER_SIZE = (PYXEL_SIZE, PYXEL_SIZE)
PLAYER_SPAWN = ((SCREEN_SIZE[0] - PYXEL_SIZE) // 2, (SCREEN_SIZE[1] - PYXEL_SIZE) // 2)
PLAYER_HEART = 300
PLAYER_MANA = 100

# TOOLS STATS
HEART_GIVE = PLAYER_HEART // 3
MANA_GIVE = 30

# PROGRESSION
PROGRESSION = [("weapon", ""), ("item", "tower"), ("item", "shield"), ("item", "teleport"), ("weapon", "slow"),
               ("item", "bomb"), ("item", "beacon_heal"), ("item", "beacon_mana"), ("item", "beacon_damage"),
               ("weapon", "poison"), ("item", "tower_slow"), ("item", "beacon_speed"), ("item", "beacon_slow"),
               ("item", "beacon_poison"), ("item", "tower_poison"), ("weapon", "burn"), ("item", "tower_burn")]


def save_change():
    with open("option.csv", "w+") as file:
        for key, val in KEYS.items():
            file.write(f"{key};{val}\n")


def different_name(tab, names):
    return [entity for entity in tab if entity.name not in names]


def collision(tab, coord):  # coord = [x, y, w, h] of the player
    for entity in tab:
        already = False
        for x_e in range(entity.x, entity.x + entity.width):
            if already:
                break
            for x in range(coord[0], coord[0] + coord[2]):
                if x_e == x:
                    for y_e in range(entity.y, entity.y + entity.height):
                        for y in range(coord[1], coord[1] + coord[3]):
                            if y_e == y:
                                return True
                    already = True
                    break
    return False
