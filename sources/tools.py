# my classes
from parameters import *


class Effect:
    def __init__(self, name, entity, time=30):
        self.name = name
        self.entity = entity

        self.reset_time = time
        self.time = self.reset_time

        self.memory = []

        if self.name == "speed":
            if hasattr(entity, "speed"):
                self.memory.append(self.entity.speed)
                self.entity.speed = self.entity.speed * 3 // 2
            self.memory.append(self.entity.weapon.reset_time)
            self.entity.weapon.reset_time -= 1
        elif self.name == "damage":
            self.memory.append(self.entity.weapon.damage)
            self.entity.weapon.damage *= 2
        elif self.name == "slow":
            if hasattr(entity, "speed"):
                self.memory.append(self.entity.speed)
                self.entity.speed //= 2
                if self.entity.speed == 0:
                    self.entity.speed = 1
            self.memory.append(self.entity.weapon.reset_time)
            self.entity.weapon.reset_time *= 2

    def update(self):
        if self.time <= 0:
            if self.name == "speed":
                if hasattr(self.entity, "speed"):
                    self.entity.speed = self.memory[0]
                    self.entity.weapon.reset_time = self.memory[1]
                else:
                    self.entity.weapon.reset_time = self.memory[0]
            elif self.name == "damage":
                self.entity.weapon.damage = self.memory[0]
            elif self.name == "slow":
                if hasattr(self.entity, "speed"):
                    self.entity.speed = self.memory[0]
                    self.entity.weapon.reset_time = self.memory[1]
                else:
                    self.entity.weapon.reset_time = self.memory[0]
            self.entity.effects.remove(self)
        elif self.name in ["heal", "mana", "poison", "burn"] and pxl.frame_count % 30 == 0:
            if self.name == "heal":
                for effect in self.entity.effects:
                    if effect.name == "poison":
                        effect.time = 0
                self.entity.life += 5
                if self.entity.life > self.entity.max_life:
                    self.entity.life = self.entity.max_life
            elif self.name == "mana":
                self.entity.mana += 1
                if self.entity.mana > self.entity.max_mana:
                    self.entity.mana = self.entity.max_mana
            elif self.name == "poison":
                self.entity.life -= 5
            elif self.name == "burn":
                self.entity.life -= 10

        self.time -= 1

    def display(self, x, y):
        if self.name == "heal":
            pxl.blt(x, y, 0, PYXEL_SIZE * 5, PYXEL_SIZE * 5, PYXEL_SIZE, PYXEL_SIZE, 3)
        elif self.name == "mana":
            pxl.blt(x, y, 0, PYXEL_SIZE * 5, PYXEL_SIZE * 6, PYXEL_SIZE, PYXEL_SIZE, 3)
        elif self.name == "damage":
            pxl.blt(x, y, 0, PYXEL_SIZE * 5, PYXEL_SIZE * 7, PYXEL_SIZE, PYXEL_SIZE, 3)
        elif self.name == "speed":
            pxl.blt(x, y, 0, PYXEL_SIZE * 5, PYXEL_SIZE * 8, PYXEL_SIZE, PYXEL_SIZE, 3)
        elif self.name == "slow":
            pxl.blt(x, y, 0, PYXEL_SIZE * 5, PYXEL_SIZE * 9, PYXEL_SIZE, PYXEL_SIZE, 3)
        elif self.name == "poison":
            pxl.blt(x, y, 0, PYXEL_SIZE * 5, PYXEL_SIZE * 10, PYXEL_SIZE, PYXEL_SIZE, 1)
        elif self.name == "burn":
            pxl.blt(x, y, 0, PYXEL_SIZE * 5, PYXEL_SIZE * 11, PYXEL_SIZE, PYXEL_SIZE, 3)

        # display the time left of the effect
        pxl.dither(0.5)
        if self.time <= 2:
            pxl.blt(x, y, 0, PYXEL_SIZE * 5, PYXEL_SIZE * 4, PYXEL_SIZE, PYXEL_SIZE, 3)
        elif self.time <= self.reset_time // 4:
            pxl.blt(x, y, 0, PYXEL_SIZE * 5, PYXEL_SIZE * 3, PYXEL_SIZE, PYXEL_SIZE, 3)
        elif self.time <= self.reset_time // 2:
            pxl.blt(x, y, 0, PYXEL_SIZE * 5, PYXEL_SIZE * 2, PYXEL_SIZE, PYXEL_SIZE, 3)
        elif self.time <= self.reset_time // 4 * 3:
            pxl.blt(x, y, 0, PYXEL_SIZE * 5, PYXEL_SIZE, PYXEL_SIZE, PYXEL_SIZE, 3)
        pxl.dither(1)


class Bullet:
    def __init__(self, name, x, y, width, height, speed_x, speed_y, damage, effect):
        self.name = name
        self.x, self.y = x, y
        self.width, self.height = width, height
        self.speed_x, self.speed_y = speed_x, speed_y

        # stats
        self.damage = damage
        self.effects = effect

    def move(self):
        self.x += self.speed_x
        self.y += self.speed_y

    def display(self):
        # display the bullet
        if self.name in ["player", "tower"]:
            if self.effects == []:
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 4 + PYXEL_SIZE // 3, PYXEL_SIZE + PYXEL_SIZE // 3, self.width,
                        self.height, 3)
            else:
                for effect in self.effects:
                    if effect[0] == "poison":
                        pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 4 + PYXEL_SIZE // 3, PYXEL_SIZE * 2 + PYXEL_SIZE // 3,
                                self.width,
                                self.height, 1)
                        break
                    elif effect[0] == "burn":
                        pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 4 + PYXEL_SIZE // 3, PYXEL_SIZE * 3 + PYXEL_SIZE // 3,
                                self.width,
                                self.height, 3)
                        break
                    elif effect[0] == "slow":
                        pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 4 + PYXEL_SIZE // 3, PYXEL_SIZE * 4 + PYXEL_SIZE // 3,
                                self.width,
                                self.height, 3)
                        break
        elif self.name == "shooter":
            pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 3 + PYXEL_SIZE // 3, PYXEL_SIZE + PYXEL_SIZE // 3, self.width,
                    self.height, 3)
        elif self.name == "sorcerer":
            for effect in self.effects:
                if effect[0] == "poison":
                    pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 3 + PYXEL_SIZE // 8, PYXEL_SIZE * 2 + PYXEL_SIZE // 8,
                            self.width, self.height, 1)
                    break
                elif effect[0] == "burn":
                    pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 3 + PYXEL_SIZE // 8, PYXEL_SIZE * 3 + PYXEL_SIZE // 8,
                            self.width, self.height, 3)
                    break
                elif effect[0] == "slow":
                    pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 3 + PYXEL_SIZE // 8, PYXEL_SIZE * 4 + PYXEL_SIZE // 8,
                            self.width, self.height, 3)
                    break


class Weapon:
    def __init__(self, name, entity, map, player):
        self.name = name  # name of the object
        self.entity = entity  # entity that have the object

        self.map = map
        self.monsters = self.map[0]
        self.items = self.map[2]
        self.player = player

        # stats
        self.effects = []

        self.tab_bullets = []

        if type(self.entity) is Item:
            self.item()
        else:
            if self.entity.name == "player":
                if self.name == "burn":
                    self.effects.append(("burn", 90))
                elif self.name == "slow":
                    self.effects.append(("slow", 60))
                elif self.name == "poison":
                    self.effects.append(("poison", 150))
                self.damage = 10
                self.reset_time = 5
            elif self.entity.name == "biscuit":
                self.damage = 10
                self.reset_time = 20
                self.range = 2
            elif self.entity.name == "shooter":
                self.tab_bullets = map[3]
                self.damage = 25
                self.reset_time = 30
                self.range = 140
            elif self.entity.name == "sorcerer":
                self.tab_bullets = map[3]
                self.damage = 10
                self.reset_time = 90
                self.range = 180

    def item(self):
        if self.entity.name == "tower":
            self.range = 150
            self.damage = 5
            self.reset_time = 5
        elif self.entity.name == "shield":
            self.damage = 10
            self.reset_time = 30
        elif self.entity.name == "teleport":
            self.damage = 0
            self.reset_time = 150  # 5 sec
        elif self.entity.name == "bomb":
            self.damage = 200
            self.reset_time = 90  # 3 sec
        elif self.entity.name[:6] == "beacon":
            self.damage = 0
            self.reset_time = 30  # 1 sec

    def pl(self):
        # update the bullets
        for bullet in self.tab_bullets:
            bullet.move()
            if not (0 < bullet.x < SCREEN_SIZE[0] and 0 < bullet.y < SCREEN_SIZE[1]):
                self.tab_bullets.remove(bullet)
            else:  # damage a monster
                for monster in self.monsters:
                    if collision([monster], [bullet.x, bullet.y, bullet.width, bullet.height]):
                        monster.life -= bullet.damage
                        for b_effect in bullet.effects:
                            for effect in monster.effects:
                                if effect.name == b_effect[0]:
                                    effect.time = 0
                                    effect.update()
                                    break
                            monster.effects.append(Effect(b_effect[0], monster, b_effect[1]))
                        self.tab_bullets.remove(bullet)
                        break

        # shoot a bullet
        if pxl.btnp(pxl.MOUSE_BUTTON_LEFT, False, self.reset_time):
            x, y = self.entity.x + self.entity.width // 2, self.entity.y + self.entity.height // 2
            dist = pxl.sqrt((pxl.mouse_x - x) ** 2 + (pxl.mouse_y - y) ** 2)
            if dist > PYXEL_SIZE // 2:
                self.tab_bullets.append(Bullet("player", x - 2, y - 2, 8, 8, int((pxl.mouse_x - x) / (dist / 7)),
                                               int((pxl.mouse_y - y) / (dist / 7)), self.damage, self.effects))
                self.entity.attacked = True

        elif self.entity.attacked:
            self.entity.attacked = False

    def biscuit(self):
        if self.entity.can_attack():
            for e in [self.player] + self.items:
                if collision([e], [self.entity.x - self.range, self.entity.y - self.range,
                             self.entity.width + self.range * 2, self.entity.height + self.range * 2]):
                    e.life -= self.damage
                    for effect in self.effects:
                        e.effects.append(Effect(effect[0], e, effect[1]))
                    self.entity.attacked = 5
                    break

    def shooter(self):
        if self.entity.can_attack():
            x, y = self.entity.x + self.entity.width // 2, self.entity.y + self.entity.height // 2
            dist, aim_x, aim_y = None, None, None
            for entity in [self.player] + self.items:
                temp_dist = pxl.sqrt((entity.x - x) ** 2 + (entity.y - y) ** 2)
                if dist is None or temp_dist < dist:
                    dist = temp_dist
                    aim_x, aim_y = entity.x + entity.width // 2, entity.y + entity.height // 2

            # shoot
            if dist is not None and dist <= self.range:
                if dist > PYXEL_SIZE // 2:
                    self.entity.attacked = 8
                    self.tab_bullets.append(
                        Bullet(self.entity.name, x - 2, y - 2, 8, 8, int((aim_x - x) / (dist / 7)),
                               int((aim_y - y) / (dist / 7)), self.damage, []))

    def sorcerer(self):
        if self.entity.can_attack():
            x, y = self.entity.x + self.entity.width // 2, self.entity.y + self.entity.height // 2
            dist, aim_x, aim_y = None, None, None
            for entity in [self.player] + self.items:
                temp_dist = pxl.sqrt((entity.x - x) ** 2 + (entity.y - y) ** 2)
                if dist is None or temp_dist < dist:
                    dist = temp_dist
                    aim_x, aim_y = entity.x + entity.width // 2, entity.y + entity.height // 2

            # shoot
            if dist is not None and dist <= self.range:
                if dist > PYXEL_SIZE // 2:
                    self.entity.attacked = 8
                    self.tab_bullets.append(
                        Bullet(self.entity.name, x - 2, y - 2, 12, 12, int((aim_x - x) / (dist / 7)),
                               int((aim_y - y) / (dist / 7)), self.damage,
                               [(self.entity.effect, self.entity.effect_time)]))

    def update(self):
        if self.entity.name == "player":
            self.pl()
        else:
            self.__getattribute__(self.entity.name)()

    def display(self):
        for bullet in self.tab_bullets:
            bullet.display()


class Item:
    def __init__(self, name, x, y, player, map):
        self.name = name
        self.x, self.y = x, y

        self.width, self.height = PYXEL_SIZE, PYXEL_SIZE

        self.player = player
        self.map = map
        self.monsters = map[0]
        self.items = map[2]

        # stats
        self.mana_cost = 0
        self.effects = []

        # attribute for the different capacities
        if self.name[:5] == "tower":
            self.effect = self.name[6:]
            if self.effect != "":
                if self.effect == "poison":
                    self.effect_time = 120
                elif self.effect == "burn":
                    self.effect_time = 60
                elif self.effect == "slow":
                    self.effect_time = 150

            self.name = "tower"
            self.mana_cost = 50

            self.max_life = 100
            self.tab_bullets = []
            self.range = 150
        elif self.name == "shield":
            self.mana_cost = 30
            self.attacking = False

            self.max_life = 1000
        elif self.name == "teleport":
            self.in_game = False
            self.mana_cost = 10

            self.max_life = 100
        elif self.name == "bomb":
            self.mana_cost = 100
            self.max_life = 100
        elif self.name[:6] == "beacon":
            self.mana_cost = 20

            self.effect = self.name[7:]
            self.name = "beacon"

            if self.effect == "heal":
                self.range = 30
            elif self.effect == "mana":
                self.mana_cost = 35
                self.range = 30
            elif self.effect == "damage":
                self.range = 30
            elif self.effect == "speed":
                self.range = 100
            elif self.effect == "slow":
                self.range = 100
            elif self.effect == "poison":
                self.range = 100
            self.max_life = 25

        self.weapon = Weapon("", self, map, self.player)

        self.life = self.max_life
        self.time = self.weapon.reset_time

    def tower(self):
        # update the bullets
        for bullet in self.tab_bullets:
            bullet.move()
            if not (0 < bullet.x < SCREEN_SIZE[0] and 0 < bullet.y < SCREEN_SIZE[1]):
                self.tab_bullets.remove(bullet)

        x, y = self.x + self.width // 2, self.y + self.height // 2
        dist, aim_x, aim_y = None, None, None
        for monster in self.monsters:
            # damage a monster
            for bullet in self.tab_bullets:
                if collision([monster], [bullet.x, bullet.y, bullet.width, bullet.height]):
                    monster.life -= bullet.damage
                    for b_effect in bullet.effects:
                        for effect in monster.effects:
                            if effect.name == b_effect[0]:
                                effect.time = 0
                                effect.update()
                                break
                        monster.effects.append(Effect(b_effect[0], monster, b_effect[1]))
                    self.tab_bullets.remove(bullet)

            temp_dist = pxl.sqrt((monster.x - x) ** 2 + (monster.y - y) ** 2)
            if dist is None or temp_dist < dist:
                dist = temp_dist
                aim_x, aim_y = monster.x + monster.width // 2, monster.y + monster.height // 2

        # shoot
        if self.time == 0:
            if dist is not None and dist <= self.range:
                if self.effect == "":
                    effect = []
                else:
                    effect = [(self.effect, self.effect_time)]
                self.tab_bullets.append(Bullet("tower", x - 2, y - 2, 8, 8, int((aim_x - x) / (dist / 7)),
                                               int((aim_y - y) / (dist / 7)), self.weapon.damage, effect))
                self.time = self.weapon.reset_time
        else:
            self.time -= 1

    def shield(self):
        if self.time == 0:
            for monster in self.monsters:
                if collision([monster], [self.x - 4, self.y - 4, self.width + 8, self.height + 8]):
                    monster.life -= self.weapon.damage
                    self.time = self.weapon.reset_time
                    self.attacking = True
        else:
            self.time -= 1
            if self.time < 20:
                self.attacking = False

    def teleport(self):
        if not self.in_game:
            self.player.number_teleport += 1
            self.in_game = True

        if self.time == 0:
            if (collision([self], [self.player.x, self.player.y, self.player.width, self.player.height]) and
               self.player.number_teleport == 2):
                for item in self.items:
                    if item.name == "teleport" and item is not self:
                        if item.time == 0:
                            self.player.x, self.player.y = item.x, item.y
                            item.time = item.weapon.reset_time
                            self.time = self.weapon.reset_time
                        break
        else:
            self.time -= 1

    def bomb(self):
        if self.time < 0:
            if self.time == -8 and self in self.items:
                self.items.remove(self)
            self.time -= 1
        elif self.time == 0:
            for monster in self.monsters:
                if collision([monster], [self.x - 175 + PYXEL_SIZE // 2, self.y - 175 + PYXEL_SIZE // 2, self.width +
                                         350, self.height + 350]):
                    monster.life -= self.weapon.damage
            self.time -= 1
        else:
            self.time -= 1

    def beacon(self):
        if self.time == 0:
            # for the player
            if self.effect in ["heal", "damage", "speed"]:  # for the player and the items
                tab = [self.player] + different_name(self.items, "beacon")
            elif self.effect in ["mana"]:  # for the player
                tab = [self.player]
            else:  # against monsters
                tab = self.monsters
            for entity in tab:
                if collision([entity], [self.x - self.range, self.y - self.range, self.width + self.range * 2
                                        , self.height + self.range * 2]):
                    for effect in entity.effects:
                        if self.effect == effect.name:
                            effect.time = 0
                            effect.update()
                            break
                    entity.effects.append(Effect(self.effect, entity))
        else:
            self.time -= 1

    def update(self):
        if hasattr(self, self.name):
            self.__getattribute__(self.name)()

        for effect in self.effects:
            effect.update()

    def display(self, for_inventory=False):
        if for_inventory:  # display the cost of the spell
            txt = str(self.mana_cost)
            pxl.text(self.x + PYXEL_SIZE // 2 - len(txt) * 2, self.y - 7, txt, 5)

        if self.name == "tower":
            if self.effect == "":
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 10, PYXEL_SIZE, self.width, self.height, 3)
            elif self.effect == "poison":
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 10, PYXEL_SIZE * 2, self.width, self.height, 1)
            elif self.effect == "burn":
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 10, PYXEL_SIZE * 3, self.width, self.height, 3)
            elif self.effect == "slow":
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 10, PYXEL_SIZE * 4, self.width, self.height, 3)

            # bullets
            for bullet in self.tab_bullets:
                bullet.display()
        elif self.name == "shield":
            if self.attacking:
                pxl.circ(self.x + self.width // 2, self.y + self.height // 2, self.width // 2 + 8, 12)
            pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 6, PYXEL_SIZE, self.width, self.height, 3)
        elif self.name == "teleport":
            if self.time == 0:
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 7, PYXEL_SIZE * 3, self.width, self.height, 3)
            elif self.time < self.weapon.reset_time // 2:
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 7, PYXEL_SIZE * 2, self.width, self.height, 3)
            else:
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 7, PYXEL_SIZE, self.width, self.height, 3)
        elif self.name == "bomb":
            if self.time <= 0:
                pxl.circ(self.x + self.width // 2, self.y + self.height // 2, self.width // 2 + 200, 8)
            elif self.time > self.weapon.reset_time // 3 * 2:
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 8, PYXEL_SIZE, self.width, self.height, 3)
            elif self.time > self.weapon.reset_time // 3:
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 8, PYXEL_SIZE * 2, self.width, self.height, 3)
            else:
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 8, PYXEL_SIZE * 3, self.width, self.height, 3)
        elif self.name == "beacon":
            # display the range
            col = 7
            if self.effect == "heal":
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 9, PYXEL_SIZE, self.width, self.height, 3)
                col = 10
            elif self.effect == "mana":
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 9, PYXEL_SIZE * 2, self.width, self.height, 3)
                col = 5
            elif self.effect == "damage":
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 9, PYXEL_SIZE * 3, self.width, self.height, 3)
                col = 8
            elif self.effect == "speed":
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 9, PYXEL_SIZE * 4, self.width, self.height, 3)
                col = 1
            elif self.effect == "slow":
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 9, PYXEL_SIZE * 5, self.width, self.height, 3)
                col = 12
            elif self.effect == "poison":
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 9, PYXEL_SIZE * 6, self.width, self.height, 1)
                col = 3
            if not for_inventory:
                pxl.circb(self.x + self.width // 2, self.y + self.height // 2, self.width // 2 + self.range, col)


class Tool:
    def __init__(self, name, x, y):
        self.name = name
        self.x, self.y = x, y

        self.width, self.height = PYXEL_SIZE, PYXEL_SIZE

        # stats
        if self.name == "nuke":
            self.damage = 500

    def display(self):
        if self.name == "heart":
            if pxl.frame_count % 60 > 30:
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 3, 0, self.width, self.height, 3)
            else:
                pxl.blt(self.x, self.y - 1, 0, PYXEL_SIZE * 3, 0, self.width, self.height, 3)
        elif self.name == "mana":
            if pxl.frame_count % 60 > 30:
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 4, 0, self.width, self.height, 3)
            else:
                pxl.blt(self.x, self.y - 1, 0, PYXEL_SIZE * 4, 0, self.width, self.height, 3)
        elif self.name == "nuke":
            if pxl.frame_count % 60 > 30:
                pxl.blt(self.x, self.y, 0, PYXEL_SIZE * 5, 0, self.width, self.height, 3)
            else:
                pxl.blt(self.x, self.y - 1, 0, PYXEL_SIZE * 5, 0, self.width, self.height, 3)
