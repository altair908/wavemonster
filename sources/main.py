# my classes
from parameters import *
from waveMonster import Game
from tools import Weapon, Item


class App:
    def __init__(self):
        pxl.init(SCREEN_SIZE[0], SCREEN_SIZE[1], display_scale=3, quit_key=False, title="Wave Monster")
        pxl.load("res.pyxres")

        pxl.icon(["0000000000", "070700E080", "072F22E280", "072F22E280", "0FFFEEE820", "0FE2E82820", "0EEE228820",
                  "00E8228200", "0008228000", "0000880000"], 1, 0)
        # pxl.fullscreen(True)

        self.level = [1, 0]  # [level, xp]
        # items inventory
        nb_case_inv = self.level[0] // 3 + 2
        if nb_case_inv > 10:
            nb_case_inv = 10
        self.inventory = [[400, 50 + y * PYXEL_SIZE * 2 + y * 5, PYXEL_SIZE * 2 + 2, PYXEL_SIZE * 2 + 2, None] for y in
                          range(nb_case_inv)]

        self.selected_weapon = "weapon"
        self.game = Game(self.level, self.get_inventory(), self.selected_weapon[7:])

        self.window = 0  # 0 -> menu // 1 -> option // 2 -> game // 3 -> items

        # buttons
        self.btn_width, self.btn_height = 125, 30
        self.x, self.y = SCREEN_SIZE[0] // 2 - self.btn_width // 2, SCREEN_SIZE[1] // 2 - self.btn_height // 2
        self.text = ["Play", "New Game", "Items", "Option"]
        self.tab_buttons = [
            [self.text[i], self.x, self.y + i * self.btn_height + i * 5, self.btn_width, self.btn_height] for i in
            range(len(self.text))]

        # items scrolling
        self.y_camera = 0
        self.lower_btn = None  # (items)

        # change keys (option/items) change items
        self.select = None

        pxl.mouse(True)
        pxl.run(self.update, self.draw)

    def get_inventory(self):
        return [deepcopy(case[-1]) for case in self.inventory]

    def update(self):
        # update the level
        xp = 150 * self.level[0]
        if self.level[1] >= xp:
            self.level[1] -= xp
            self.level[0] += 1

        # return to menu or quit
        if pxl.btnp(pxl.KEY_ESCAPE):
            if self.window != 0:
                # load the new inventory
                if self.window == 3:
                    self.game = Game(self.level, self.get_inventory(), self.selected_weapon[7:])

                pxl.camera(0, 0)
                self.window = 0
                self.tab_buttons = [
                    [self.text[i], self.x, self.y + i * self.btn_height + i * 5, self.btn_width, self.btn_height] for i
                    in range(len(self.text))]
            else:
                pxl.quit()

        if self.window == 0:  # menu
            if pxl.btnp(pxl.MOUSE_BUTTON_LEFT):
                # press a button
                for btn in self.tab_buttons:
                    if btn[1] <= pxl.mouse_x <= btn[1] + btn[3] and btn[2] <= pxl.mouse_y <= btn[2] + btn[4]:
                        if btn[0] == "Play":
                            self.window = 2
                            self.tab_buttons = None
                        elif btn[0] == "New Game":
                            self.game = Game(self.level, self.get_inventory(), self.selected_weapon[7:])
                            self.window = 2
                            self.tab_buttons = None
                        elif btn[0] == "Items":
                            self.window = 3
                            possible_items = [PROGRESSION[i] for i in range(self.level[0] + 2) if i < len(PROGRESSION)]
                            self.tab_buttons = []
                            x, y_w, y_i = 5, 5, 5
                            for i in range(len(possible_items)):
                                if possible_items[i][0] == "weapon":
                                    self.tab_buttons.append([possible_items[i][0] + " " + possible_items[i][1], x + self.btn_width + 5,
                                                 y_w, self.btn_width, self.btn_height, False])
                                    y_w += self.btn_height + 5
                                else:
                                    self.tab_buttons.append([Item(possible_items[i][1], x + self.btn_width // 2 - PYXEL_SIZE // 2,
                                                 y_i + 4 + self.btn_height // 2 - PYXEL_SIZE // 2, self.game.player, self.game.map), x, y_i,
                                                 self.btn_width, self.btn_height, False])
                                    y_i += self.btn_height + 5
                            self.lower_btn = max(y_w, y_i)
                        elif btn[0] == "Option":
                            self.window = 1
                            tab_keys_val = [[key, val] for key, val in KEYS.items()]

                            self.tab_buttons = [[tab_keys_val[y], 5, y * self.btn_height + y * 2 + 60, self.btn_width, self.btn_height, False]
                                                if y < len(KEYS) // 2 else [tab_keys_val[y], SCREEN_SIZE[0] //
                                                2 + 4, (y - len(KEYS) // 2) * self.btn_height + (y - len(KEYS) // 2) * 2
                                                + 60, self.btn_width, self.btn_height, False] for y in range(len(KEYS))]

        elif self.window == 1:  # option
            # press a button
            if pxl.btnp(pxl.MOUSE_BUTTON_LEFT):
                i = 0
                for btn in self.tab_buttons:
                    if btn[1] <= pxl.mouse_x <= btn[1] + btn[3] and btn[2] <= pxl.mouse_y <= btn[2] + btn[4]:
                        self.select = (btn[0][0], i)
                        btn[5] = True
                    i += 1

            # change a key
            if self.select is not None:
                tab_keys = ["KEY_A", "KEY_B", "KEY_C", "KEY_D", "KEY_E", "KEY_F", "KEY_G", "KEY_H", "KEY_I", "KEY_J",
                            "KEY_K", "KEY_L", "KEY_M", "KEY_N", "KEY_O", "KEY_P", "KEY_Q", "KEY_R", "KEY_S", "KEY_T",
                            "KEY_U", "KEY_V", "KEY_W", "KEY_X", "KEY_Y", "KEY_Z", "KEY_1", "KEY_2", "KEY_3", "KEY_4",
                            "KEY_5", "KEY_6", "KEY_7", "KEY_8", "KEY_9", "KEY_0", "KEY_SPACE", "KEY_SHIFT", "KEY_TAB",
                            "KEY_ALT", "KEY_CTRL", "KEY_LEFT", "KEY_UP", "KEY_RIGHT", "KEY_DOWN"]
                for key in tab_keys:
                    if pxl.btnp(pxl.__getattribute__(key)):
                        KEYS[self.select[0]] = key
                        self.tab_buttons[self.select[1]][0][1] = key
                        self.tab_buttons[self.select[1]][5] = False
                        self.select = None

                        # save the new key
                        save_change()
        elif self.window == 3:
            if self.lower_btn - SCREEN_SIZE[1] >= self.y_camera - pxl.mouse_wheel * 7 >= 0:
                self.y_camera -= pxl.mouse_wheel * 7
                pxl.camera(0, self.y_camera)

            if pxl.btnp(pxl.MOUSE_BUTTON_LEFT):
                # reset selected item
                for btn in self.tab_buttons:
                    if btn[5]:
                        btn[5] = False

                if self.select is not None:
                    # select the wepon
                    x, y = self.inventory[0][0] - self.btn_width // 2 + PYXEL_SIZE, self.inventory[0][
                        1] - self.btn_height - 5
                    if x <= pxl.mouse_x <= x + self.btn_width and y <= pxl.mouse_y <= y + self.btn_height:
                        self.selected_weapon = self.select
                    else:
                        # add the item to the inventory
                        for case in self.inventory:
                            if case[0] <= pxl.mouse_x <= case[0] + case[2] and\
                                    case[1] - self.y_camera <= pxl.mouse_y <= case[1] - self.y_camera + case[3]:
                                if type(self.select) is Item:
                                    if hasattr(self.select, "effect"):
                                        name = self.select.name + "_" + self.select.effect
                                    else:
                                        name = self.select.name
                                    case[4] = Item(name, case[0] + PYXEL_SIZE // 2, case[1] + PYXEL_SIZE // 2 + 4,
                                                   self.select.player, self.select.map)
                                break
                    self.select = None

                # select
                for btn in self.tab_buttons:
                    if btn[1] <= pxl.mouse_x <= btn[1] + btn[3] and \
                            btn[2] - self.y_camera <= pxl.mouse_y <= btn[2] - self.y_camera + btn[4]:
                        self.select = btn[0]
                        btn[5] = True
        else:  # game
            self.game.update()

    def draw(self):
        if self.window == 0:  # menu
            # background
            pxl.cls(3)

            # display the title
            txt = "WAVE MONSTER"
            pxl.rect(SCREEN_SIZE[0] // 2 - len(txt) * 4, self.y // 2, len(txt) * 8,
                     self.btn_height, 11)
            pxl.text(SCREEN_SIZE[0] // 2 - len(txt) * 2, self.y // 2 + self.btn_height // 2 - 2, txt, 7)

            for btn in self.tab_buttons:
                pxl.rect(btn[1], btn[2], btn[3], btn[4], 9)
                pxl.rect(btn[1] + 2, btn[2] + 2, btn[3] - 4, btn[4] - 4, 10)
                pxl.text(btn[1] + btn[3] // 2 - len(btn[0]) * 2, btn[2] + btn[4] // 2 - 2, btn[0], 7)

        elif self.window == 1:
            pxl.cls(3)

            txt = "OPTION"
            pxl.rect(SCREEN_SIZE[0] // 2 - len(txt) * 4, 23 - self.btn_height // 2, len(txt) * 8,
                     self.btn_height, 11)
            pxl.text(SCREEN_SIZE[0] // 2 - len(txt) * 2, 20, txt, 7)

            for btn in self.tab_buttons:
                pxl.rect(btn[1], btn[2], btn[3], btn[4], 9)
                if not btn[5]:
                    pxl.rect(btn[1] + 2, btn[2] + 2, btn[3] - 4, btn[4] - 4, 10)
                    # in the case
                    pxl.text(btn[1] + btn[3] // 2 - len(btn[0][1][4:]) * 2, btn[2] + btn[4] // 2 - 2, btn[0][1][4:], 7)
                else:
                    # in the case
                    txt = "press any key"
                    pxl.text(btn[1] + btn[3] // 2 - len(txt) * 2, btn[2] + btn[4] // 2 - 2, txt, 7)
                # at the right of the case
                pxl.text(btn[1] + btn[3] + btn[3] // 4 - len(btn[0][0]) * 2, btn[2] + btn[4] // 2 - 2, btn[0][0], 7)
        elif self.window == 3:  # items
            pxl.cls(3)

            # display the inventory
            for case in self.inventory:
                pxl.rect(case[0], case[1], case[2], case[3], 9)
                pxl.rect(case[0] + 2, case[1] + 2, case[2] - 4, case[3] - 4, 10)
                if case[4] is not None:
                    case[4].display(True)
            # display the weapon
            x, y = self.inventory[0][0] - self.btn_width // 2 + PYXEL_SIZE, self.inventory[0][1] - self.btn_height - 5
            pxl.rect(x, y, self.btn_width, self.btn_height, 9)
            pxl.rect(x + 2, y + 2, self.btn_width - 4, self.btn_height - 4, 10)
            pxl.text(x + self.btn_width // 2 - len(self.selected_weapon) * 2, y + self.btn_height // 2 - 2, self.selected_weapon, 7)

            # display the buttons
            for btn in self.tab_buttons:
                pxl.rect(btn[1], btn[2], btn[3], btn[4], 9)
                if not btn[5]:
                    pxl.rect(btn[1] + 2, btn[2] + 2, btn[3] - 4, btn[4] - 4, 10)
                if type(btn[0]) is Item:
                    btn[0].display(True)
                else:
                    pxl.text(btn[1] + btn[3] // 2 - len(btn[0]) * 2, btn[2] + btn[4] // 2 - 2, btn[0], 7)
        else:  # game
            pxl.cls(0)
            self.game.draw()


App()
