# waveMonster


## Description
It's a little game made with Python where the goal is to survive. Monsters will spawn on the map and you have to kill them all before they kill you !
You can have a lot of competences like the towers, the shield, the beacons and more.

## Instalation
To run this project you'll need an interpretor for Python and install the module name PYXEL (use to display the game).

* Install Pyxel on **Linux**:  
    * First download the files
    * Then create a .venv in the source directory by doing: **sudo apt install python3-venv; python3 -m venv .venv**
    * activate the .venv: **source .venv/bin/activate**
    * install pyxel in the .venv: **pip install pyxel**
    * run the project: **python3 main.py**
    * dont forget to deactivate the .venv: **deactivate**

## Usage
You can move your character using the zqsd buttons and you can shoot projectiles with left click.
However, you can change the control buttons in the options.

## Advancement
This project isn't finish yet. But it's pretty advanced, so have fun with it :)
